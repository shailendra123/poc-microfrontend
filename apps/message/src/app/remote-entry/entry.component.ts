import { Component } from '@angular/core';

@Component({
  selector: 'practice-message-entry',
  templateUrl: '../app.component.html',
  styles: [
    `
      .remote-entry {
        background-color: #143055;
        color: white;
        padding: 5px;
      }
    `,
  ],
})
export class RemoteEntryComponent {}
